Outcomes for this lab:
- Create arbitrary spans and send them to an opentelemetry-enabled backend
- Understand the different types of span that can be created
- Understand trace attributes and how they are propagated


The Astronomy Shop Demo is great, but there's a little too much going on to really dive into the details of the protocol. Let's scale things down.

Logs and Metrics are great, but Traces are really where OpenTelemetry shines, and is the future of observability. Let's look at the approach OpenTelemetry is taking with traces and why they're so powerful.

In this lab we're going to explore a tool called `otel-cli` that lets us manually create spans and traces in the OpenTelemetry Protocol to better understand how they work.

## 1. Install the dependencies and get set up

You'll need a [working Go installation](https://go.dev/doc/install).

```
go install github.com/CtrlSpice/otel-desktop-viewer@latest
go install github.com/equinix-labs/otel-cli@latest
otel-desktop-viewer
```

The UI is viewable on port 8000. Forward to that port and take a look.

click the + at the top of the terminal to open a new tab.

We need to set an environment variable to tell `otel-cli` where to send the data. `otel-desktop-viewer` is listening on port 4318.

```
export OTEL_EXPORTER_OTLP_ENDPOINT=http://localhost:4318
```

## 2. Create your first span

A span is a single unit of work it your system. It may be handling a web request, performing an I/O operation like a database lookup, or triggering a function call.
Traces are made up of related spans. Spans may be children of an overarching "Traceparent" propagating context from one span to the next. 

Let's create a single span called "curl google". `otel-cli exec` runs the command you pass it, and wraps it in a span and passes relevant context.

```
$ otel-cli exec --service my-service --name "curl google" curl https://google.com
```

Take a look at the span in the otel-desktop-viewer UI. See how much context was sent along with the timing?

## 3. A trace made up of two spans

Now let's create two spans, part of the same trace.

```
$ otel-cli exec -n span-1 -- otel-cli exec -n span-2 curl https://google.com
```

The first span propagates its context and relationship to the next service that generates the second span.


## 4. A Span of a different kind

The OpenTelemetry Specification mentions different ["Kinds" of spans](https://opentelemetry.io/docs/concepts/signals/traces/#span-kind), based on the type of work, and type of entity, that generates them. There are:

* Producer
* Consumer
* Client
* Server
* Internal

Let's generate some spans of different kinds.

```sh
carrier=$(mktemp)
otel-cli span -s $0 -n "traceparent demo" --tp-print --tp-carrier $carrier 

# this will start a child span, and run another otel-cli as its program
otel-cli exec \
	--service    "fake-client" \
	--name       "hammer the server for sweet sweet data" \
	--kind       "client" \
	--tp-carrier $carrier \
	--verbose \
     	--fail \
	-- \
	otel-cli exec -n fake-server -s 'put up with the clients nonsense' --kind server /bin/echo 500 NOPE

otel-cli exec \
	--service    "fake-producer" \
	--name       "churn out data for the downstream service" \
	--kind       "producer" \
	--tp-carrier $carrier \
	--verbose \
     	--fail \
	-- \
	otel-cli exec -n fake-consumer -s 'slurp up all that data' --kind server /bin/echo yum yum yum
```

## 5. It's all about the attributes

The OpenTelemetry Spec also lets you tag your spans with key-value pairs, called "Span Attributes", with useful information.


```sh
$ otel-cli span --attrs clientId=alex,companyId=none,freesoftware=awesome
```

You can query your traces by these attributes.


## 6. Cleanup

Press Q to shut down `otel-desktop-viewer`

If you want to view the JSON of the spans directly, `otel-cli` will write them to a directory:

```sh
$ otel-cli server json --dir ./ --timeout 60 --max-spans 5
```
