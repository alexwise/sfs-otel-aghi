Outcomes for this lab:
- Stand up a new tracing backend (Zipkin)
- Modify the otel-collector config to pipeline data to the new backend
- Navigate Zipkin and query for traces

Let's play with the Astronomy Shop demo again. But first, let's make some changes.
We've heard that Zipkin is a powerful way to visualize traces and service dependencies, and want to see what our data looks like in it
But we don't want to give up our awesome Jaeger UI that we know and love.
Fortunately, OpenTelemetry has this solved.

## Stand up the Zipkin Infrastructure

Open up the text editor, and navigate to the `docker-compose.yml` file.

[Zipkin](https://zipkin.io/) is another libre tracing backend. Let's set Zipkin up as another exporter for our trace data.

Copy this block into the "Telemetry Components" section of the Docker Compose file, somewhere around line 722
This creates a Zipkin environment on the same Docker network as the rest of the demo.

```
  zipkin-storage:
    image: openzipkin/zipkin-mysql
    container_name: zipkin-mysql

  # The zipkin process services the UI, and also exposes a POST endpoint that
  # instrumentation can send trace data to. Scribe is disabled by default.
  zipkin:
    image: openzipkin/zipkin
    container_name: zipkin
    # Environment settings are defined here https://github.com/openzipkin/zipkin/blob/master/zipkin-server/README.md#environment-variables
    environment:
      - STORAGE_TYPE=mysql
      # Point the zipkin at the storage backend
      - MYSQL_HOST=zipkin-mysql
      - MYSQL_USER=zipkin
      - MYSQL_PASS=zipkin      
    ports:
      # Port used for the Zipkin UI and HTTP Api
      - 9411:9411
      # Uncomment if you set SCRIBE_ENABLED=true
      # - 9410:9410
    depends_on:
      - zipkin-storage
  zipkin-dependencies:
    image: openzipkin/zipkin-dependencies
    container_name: dependencies
    entrypoint: crond -f
    environment:
      - STORAGE_TYPE=mysql
      - MYSQL_HOST=mysql
      # Add the baked-in username and password for the zipkin-mysql image
      - MYSQL_USER=zipkin
      - MYSQL_PASS=zipkin
    depends_on:
      - zipkin-storage
```

## Update the otel-collector to start sending trace data

Now let's update the otel-collector config to use our new Zipkin instance:

```
exporters:
  debug:
  otlp:
    endpoint: "jaeger:4317"
    tls:
      insecure: true
  otlp/logs:
    endpoint: "dataprepper:21892"
    tls:
      insecure: true
  otlphttp/prometheus:
    endpoint: "http://prometheus:9090/api/v1/otlp"
    tls:
      insecure: true
  zipkin/nontls:
    endpoint: "http://zipkin:9411/api/v2/spans"
    format: proto
    default_service_name: unknown-service

...

service:
  pipelines:
    traces:
      receivers: [otlp]
      processors: [batch]
      exporters: [otlp, debug, spanmetrics, zipkin/nontls]
```

## And let's fire it up:

```
$ docker compose up --force-recreate --remove-orphans --detach --wait --wait-timeout 500
```

Port forward to port 9411 to take a look at the Zipkin UI

Congratulations! You've just added a new tracing provider to your microservices environment!