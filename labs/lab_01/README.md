Outcomes for this lab:
- A working Astronomy Shop microservices environment, instrumented for observability
- Perform a checkout
- Be able to explain the role of the OpenTelemetry Collector, and how it is configured.
- Able to access and query Prometheus, OpenSearch, and Jaeger, and understand the type of information you get from each



# Lab 01 - Running the OpenTelemetry Astronomy Shop
## 1.  Clone the `opentelemetry-demo` repo:
```
$ cd ~
$ git clone https://github.com/open-telemetry/opentelemetry-demo.git && cd opentelemetry-demo
```

## 2. Let's take a look at the OpenTelemetry Collector config
  It's located at `src/otelcollector/otel-config.yml`

```
/home/alexander_c_wise/opentelemetry-demo/src/otelcollector/
├── [-rw-r--r--]  otelcol-config-extras.yml
└── [-rw-r--r--]  otelcol-config.yml
```

Let's open it in the text editor and take a look.
Click on "Open Editor" in the upper right of the terminal window and navigate to the otelcol-config.yml file.

Notice the first three blocks in the file are "Receivers", "Processors", and "Exporters". These are the three basic functions of the collector.

Multiple Receivers, Processors, and Exporters are configured for each of these. To learn about any of these, check the receiver/processor/exporter directories in the [source code.](https://github.com/open-telemetry/opentelemetry-collector-contrib)

Scroll down to the pipeline block. This shows how the collectors can be configured to receive and deliver data to multiple sources for Logs, Metrics, and Traces.

For example, Tracing data comes in directly from services in the OpenTelemetry Line Protocol (OTLP), where it is batched and sent to 

## 3. Stand it up with `docker compose`
```
$ docker compose up --force-recreate --remove-orphans --detach --wait --wait-timeout 500
```

You should see the following if everything is healthy (it may take a few minutes):

![Docker output of the Astronomy Shop container coming up healthy](../images/dockercompose.png)


## 4. Congratulations! The Astronomy Shop is up and running. Let's take a look at how it works

All services are exposed through port 8080. Forward that port and browse to start shopping.

- You can actually order something. Add an item to your cart and check out.

- The demo has a load generator configured to simulate customer activity. Browse to "/loadgen" to view Locust

- Let's take a look at some metrics. Prometheus metrics can be visualized in Grafana dashboard. Browse to "/grafana" to take a look.
  - Click on "Browse -> Dashboards" on the sidebar
  - Click "OpenTelemtry Demo"

- The Astronomy Shop Demo does generate and store logs in OpenSearch, but they are not exposed and difficult to query. We'll skip them until Lab 06, where we set up a dashboard to view and query them.

- Now let's check traces. Traces are first-class in the OTel demo, and provide everything you need to understand the system.
  Traces are visible in Jaeger. Let's take a look at Jaeger. Browse to "jaeger/ui"
  Let's find a trace, Select "frontend" from the Service dropdown.

- The Astronomy Shop comes with some built-in bugs we can toggle on and off and see them show up in our monitoring. Browse to "/feature" and toggle on the "cartServiceFailure" flag.

Let's take a look in Jaeger to see if we can see those failures happening. Browse to "/jaeger/ui" and search for spans from the "frontend" service with the tag "error=true"

![Jaeger query for erroring logs](../images/jaegerquery.png)


You can [dive deeper](https://opentelemetry.io/docs/demo/scenarios/recommendation-cache/) if you care to learn more about investigating application behavior with OTel.

Now, let's tear it all down. If we really want to learn about 

```sh
$ docker compose stop
```