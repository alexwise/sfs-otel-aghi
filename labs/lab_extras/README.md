## Add an OpenSearch Dashboard to query logs.

While the Astronomy Shop does send logs to an OpenSearch cluster, it does not give you an easy way to view or query them directly.

We can stand up the OpenSearch Dashboard UI to get a better idea of what logs are being stored.

Update `docker-compose.yml` to include the following block:

```
  opensearch-dashboards:
    image: opensearchproject/opensearch-dashboards:latest # Make sure the version of opensearch-dashboards matches the version of opensearch installed on other nodes
    container_name: opensearch-dashboards
    ports:
      - 5601:5601 # Map host port 5601 to container port 5601
    expose:
      - "5601" # Expose port 5601 for web access to OpenSearch Dashboards
    environment:
      OPENSEARCH_HOSTS: '["https://opensearch:9200"]' # Define the OpenSearch nodes that OpenSearch Dashboards will query
```
and stand up the Astronomy shop...

```
$ make start
```

You can now port forward to 5601 to view the UI.