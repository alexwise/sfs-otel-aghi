Hello Everybody, welcome to Lab 00! 

Outcomes for this lab:
- A working environment capable of running the labs
- Downloading all of the class materials to the instance (what you're reading now!)
- Confirmation that necessary dependencies are available.

In this lab we'll get the environment set up and dependencies installed. The OpenTelemetry Astronomy Shop Demo is used in labs 1 & 5. It is beefy, and can be a taxing for some machines. It needs 6GB of memory, and if it does not have enough compute you may see stalled, crashing, and dropped data. If you have a machine that supports these requirements, make sure you have a modern [Docker Compose installation](https://docs.docker.com/compose/install/linux/), and [Go installed](https://go.dev/doc/install). 

If you do not have a machine that meets these requirements, you can sign up for a free trial at [GitPod](gitpod.io) to get access to a cloud environment that (mostly) meets these specifications.

You can also use [Google Cloud Shell](https://cloud.google.com/shell). It requires at a credit card set up for a billing account, but no charges will be incurred for using the cloud shell. It is certainly less libre than GitPod, and struggles to run the Astronomy Shop demo for extended periods of time.

Both GitPod and Google Cloud Shell are preconfigured with all the software dependencies needed for these labs.

## 1. Pull the git repository containing the class materials:
```
$ git clone https://gitlab.com/alexwise/sfs-otel-aghi.git && cd sfs-otel-aghi/labs/lab_00
```

## 2. Test that required dependencies are installed

```sh
$ go --version

$ go env gopath
```

```sh
$ docker compose up


$ docker compose down
```


Great! You're all set to go. We'll be making this environment sweat in the next lab, where we stand up a working ecommerce site with 20 services all instrumented with OpenTelemetry.