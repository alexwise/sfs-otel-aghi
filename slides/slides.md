---
# general Pandoc options
lang: en-US # change e.g. to 'de-CH' for German (Switzerland)
css: assets/custom.css # put your custom styles in this file

# title slide options
author: Alex Wise
title: OpenTelemetry
subtitle: A Gentle Hands-On Introduction
date: 18 November 2023

# Pandoc options for (reveal.js) slides
revealjs-url: node_modules/reveal.js
theme: black # for possible values see https://revealjs.com/themes/
#background-image: assets/unsplash-lERuUa_96uo.jpg # uncomment for the same background image on every slide

# reveal.js config; for more options like `parallaxBackgroundImage`, see https://revealjs.com/config/
mouseWheel: false
## native presentation size, cf. https://revealjs.com/presentation-size/
width: 1920
height: 1080

# additional reveal.js plug-in config
## progress bar of elapsed time
## see https://github.com/tkrkt/reveal.js-elapsed-time-bar#configurations
elapsedtimebar:
    enable: true
    allottedTime: 15 * 60 * 1000 # equals 15 min; unit is milliseconds
    progressBarHeight: 3 # unit is pixels
    barColor: 'rgb(200,0,0)'
    pausedBarColor: 'rgba(200,0,0,.6)'
## highlight the current mouse position with a spotlight
## see https://github.com/denniskniep/reveal.js-plugin-spotlight#configuration
spotlight:
    enable: true
    size: 60 # size of the spotlight
    lockPointerInsideCanvas: false # lock the mouse pointer inside the presentation
    togglePresentationModeKeyCode: 77 # [keyCode](https://developer.mozilla.org/docs/Web/API/KeyboardEvent/keyCode) to toggle presentation mode; visit <https://keycode.info/> to easily determine keyCodes (77 = m); disabled when set to `false`
    toggleSpotlightOnMouseDown: true # toggle spotlight by holding down the mouse key
    spotlightOnKeyPressAndHold: false # [keyCode](https://developer.mozilla.org/docs/Web/API/KeyboardEvent/keyCode) to toggle spotlight; visit <https://keycode.info/> to easily determine keyCodes; disabled when set to `false`
    spotlightCursor: 'none' # the cursor when spotlight is on; e.g. 'crosshair'
    presentingCursor: 'none' # the cursor when spotlight is off and in presentation mode; e.g. 'default'
    initialPresentationMode: true # start the presentation in presentation mode
    disablingUserSelect: true # disable text selection in presentation mode
    fadeInAndOut: 100 # transition duration; unit is milliseconds; disabled when set to `false`
    useAsPointer: false # use a pointer instead of a spotlight
    pointerColor: 'red' # only relevant if `useAsPointer: true`
## indicators to show the amount of slides in a vertical stack
## see https://github.com/Martinomagnifico/reveal.js-verticator#configuration
verticator:
    enable: true
    darktheme: true # set to `false` if you use a light theme
    color: '' # manually set the normal verticator color
    oppositecolor: '' # manually set the inverted verticator color
    skipuncounted: true # Omit drawing Verticator bullets for slides that have `data-visibility="uncounted"` set?
    clickable: true # Allow navigation to a slide by clicking on the corresponding Verticator bullet?
---

# My primary hopes for this talk

- Focus on why OpenTelemetry exists and what problems it solves
- Give hands-on experience configuring OpenTelemetry for a complex, polyglottal application
- Give hands-on experience creating OpenTelemetry traces, and understanding how they work.
- Talk through some challenges running OpenTelemetry, and what its limitations are.


# $ whoami

- SWE focused on the reliability of distributed systems
- Software Freedom School
- Learning From Incidents
- Verica/Prowler/VOID Advisory Network
- An OpenTelemetry user

## whoaminot

- An OpenTelemetry contributor
- A monitoring company salesperson
- A Deep expert of OTel internals

# A History of OpenTelemetry


# The Ancient Times
(You know, like 15 years ago)

![Look on my works, ye mighty, and despair!](images/ozymandias.png)

## There were a few ways to understand what your application was up to while it was running...

## Logs {data-transition="convex" data-visibility="uncounted"}

- A generally-accepted standard since the 80s

- Highly cardinal events

## Metrics {data-transition="convex"}

- Mostly host-level (resource use, viewed from outside the )

- Aggregateable, bucketed, time-series

# This created fertile ground for monitoring companies

Logs and Host-level Metrics are relatively easy to standardize on.

Therefore, a lot of providers popped up to help wrangle this data. 

## But then our software systems got more complex 😱

##

Logs and metrics dashboards weren't always enough to know what's going on...

# 

![What's inside the black box?](images/blackbox.png)

## Beyond the Black Box {data-transition="convex"}

> "What if we had the application tell us its view of the world? That would make it so easy to identify and debug stuff!"

## The Birth of "Observability" {data-transition="convex"}

You can no longer have a single agent that works for everyone! You need libraries _inside_ the app!

## A lot of people had this revelation {data-transition="convex"}

- Libre projects like Dapper, Zipkin, OpenTracing, OpenCensus, Prometheus

- As well as proprietary APM solutions from monitoring companies like New Relic, Elastic, and DataDog.

## But creating instrumentation for every language and framework was a lot more effort {data-transition="convex"}

![From "A Brief History of OpenTelemetry (So Far), CNCF](images/foreach.png)

# Consolidating the Sprawl

Complexity was increasing, and the amount of work to do grew exponentially. Even BigCos couldn't manage the sprawl, not to mention monitoring vendors and even popular open source projects like Prometheus.

OpenTracing (2015) and OpenCensus (2017) were founded to attempt to create an extensible standard.

## The Two Projects merged in 2019 to become OpenTelemetry

> OpenTracing and OpenCensus have led the way in that effort, and while each project made different architectural choices, **the biggest problem with either project has been the fact that there were two of them.**

_A Brief History of OpenTelemetry (So Far), https://www.cncf.io/blog/2019/05/21/a-brief-history-of-opentelemetry-so-far/_

## What exactly is it?

At its core, the OpenTelemetry project is a set of API, protocol, and data specifications that allow a variety of tools and providers to send and receive observability data.

They also contribute engineering effort to a library ecosystem to implement OTel in various languages and applications. And participate on the steering committee for some languages to drive compatibility and performance.

# Lab 00

Getting an environment set up

## Requirements

- For the labs, you'll need a linux environment with 6GB of memory and Docker Compose v2 and Golang installed. The OpenTelemetry Demo (Labs 1 & 5, is pretty beefy)

- You can use GitPod or Google Cloud Shell as a developer environment that has these dependencies pre-installed.

# The Astronomy Shop Demo

The best way to learn software is to actually build with it.
The OTel project maintains a demo application of a dozen services in different languages, that we can stand up to get an idea of what modern Observability tooling can offer

## Architecture

The Astronomy Shop Demo is [quite a thing](https://opentelemetry.io/docs/demo/architecture/){:target="_blank"}  

![The Astronomy Shop Architecture](../images/architecture.png)

It emits, stores, and visualizes Logs, Metrics, and Traces for each of these services, a duty performed by...

## The OpenTelemetry Collector

![Movie Poster for The Collector](images/thecollector.jpg)


## Sorry, This Collector

![Diagram of OTel Collector](images/otel-collector.svg)


## OTel Collector

The OpenTelemetry Collector is a containerized deployment that is able to receive data from a variety of sources, perform processing and pipelining, and transmitting it in OTLP. 

There are three types of components:

- [Receivers](https://github.com/open-telemetry/opentelemetry-collector-contrib/tree/main/receiver){:target="_blank"} (scraping Prometheus endpoints or receiving logs from a parser)
- [Processors](https://github.com/open-telemetry/opentelemetry-collector-contrib/tree/main/processor){:target="_blank"} (batching data together or limiting memory use)
- [Exporters](https://github.com/open-telemetry/opentelemetry-collector-contrib/tree/main/exporter){:target="_blank"} (Sending data to providers (Elastic, DataDog, Jaeger))

## The Architecture

![App to OTelCol](images/app-to-otelcol.png)

# Lab 01

Let's get our hands dirty with the Astronomy Shop

## Logs and OpenTelemetry

> For traces and metrics, OpenTelemetry takes the approach of a clean-sheet design, specifies a new API, and provides full implementations of this API in multiple language SDKs.
OpenTelemetry’s approach with logs is different. Because existing logging solutions are widespread in language and operational ecosystems, OpenTelemetry acts as a “bridge” between those logs, the tracing and metrics signals, and other OpenTelemetry components. In fact, the API for logs is called the “Logs Bridge API” for this reason.

_https://opentelemetry.io/docs/concepts/signals/logs/_

# The OpenTelemetry Specification (for Traces)

Traces are really the first-class citizens of OpenTelemetry, and for good reason.
They are usually the most valuable, highly-cardinal representation of what your system is doing

## The Anatomy of a Trace

<section>
  <pre><code data-trim data-noescape>
{
  "name": "hello",
  "context": {
    "trace_id": "0x5b8aa5a2d2c872e8321cf37308d69df2",
    "span_id": "0x051581bf3cb55c13"
  },
  "parent_id": null,
  "start_time": "2022-04-29T18:52:58.114201Z",
  "end_time": "2022-04-29T18:52:58.114687Z",
  "attributes": {
    "http.route": "some_route1"
  },
  "events": [
    {
      "name": "Guten Tag!",
      "timestamp": "2022-04-29T18:52:58.114561Z",
      "attributes": {
        "event_attributes": 1
      }
    }
  ]
}
  </code></pre>
</section>


## The Child Span

<section>
  <pre><code data-trim data-noescape>
{
  "name": "hello-greetings",
  "context": {
    "trace_id": "0x5b8aa5a2d2c872e8321cf37308d69df2",
    "span_id": "0x5fb397be34d26b51"
  },
  "parent_id": "0x051581bf3cb55c13",
  "start_time": "2022-04-29T18:52:58.114304Z",
  "end_time": "2022-04-29T22:52:58.114561Z",
  "attributes": {
    "http.route": "some_route2"
  },
  "events": [
    {
      "name": "hey there!",
      "timestamp": "2022-04-29T18:52:58.114561Z",
      "attributes": {
        "event_attributes": 1
      }
    },
    {
      "name": "bye now!",
      "timestamp": "2022-04-29T18:52:58.114585Z",
      "attributes": {
        "event_attributes": 1
      }
    }
  ]
}
  </code></pre>
</section>

## Other Properties of Traces

- Span Context and Span Links
  - Relationships to other Spans, implying causal relationship

- Span Kind
  - What type of service/activity generated the span

- Span Events & Span Attributes
  - Metadata about the Span, like a rich structured log

# Lab 02 




# Lab 03

## Instrumenting Third-Party Software

TODO: Graphic of a trace dying because a proxy server isn't instrumented

# Lab 04

# Lab 05

# Conclusions


# Challenges in Production
A lot of the same as other monitoring solutions

* Tradeoffs between Cardinality, Dimensionality, and Cost
* Scaling the Collector
* Maintaining Backpressure
* Autoinstrument if your language supports it
* Try to get fully e2e coverage of your requests
* Sample your traces!


# Thanks! {data-background-image=assets/unsplash-_g1WdcKcV3w.jpg}

# Extra Stuff

## There Are No Three Pillars

> I’ve often pointed out that observability is built on top of arbitrarily wide structured data blobs, and that metrics, logs, and traces can be derived from those blobs while the reverse is not true—you can’t take a bunch of metrics and reformulate a rich event.

 - Charity Majors, "THE TRUTH ABOUT “MEH-TRICS”"

## 

```md
| Logs    | Ad-hoc context of discrete events            |
|---------|----------------------------------------------|
| Metrics | Aggregations and low-cardinality time series |
|---------|----------------------------------------------|
| Traces  | Relationships between events, timings        |
```

# References